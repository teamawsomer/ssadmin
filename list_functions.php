<?php 
require'vendor/autoload.php';
$sites = array();			// For the list of sites

$sites = getNames($sites);
$sites = getUrl($sites);
$sites = getSsStatus($sites);
$sites = getGitStatus($sites);


// Gets sites name from bash script and saves them in sites array
function getNames($sites) {
  exec('/bin/bash sitenames.sh;', $output, $return);
  foreach($output as $name){		
    $sites[]["name"] = $name;
  }
  return $sites;
}

// Parses trough files in sites-enabled to get URL
// Inspiration from: stackoverflow.com
function getUrl($sites){
  $path = '/etc/apache2/sites-enabled';
  $directories = scandir($path);		                    // Gets files and directories in path
  $files =  array_diff($directories, array('..','.'));  // Removes the . and .. file
  $info = array();
  $x=0;
  
  // Goes trough files in sites-enabled
  foreach($files as $file){		                          
    $thisFile = fopen($path.'/'.$file, 'r' )or die('Cant open');
    while(!feof($thisFile)){
    	$line = fgets($thisFile);
    	$line = trim($line);
    	$tokens = explode(' ', $line);
    	if(!empty($tokens)){
    		if(strtolower($tokens[0])=='servername'){
    			$info[$x]['url'] = $tokens[1];		
        }
        if(strtolower($tokens[0])=='documentroot'){
          // basename for just getting the sitename
          $info[$x]['name'] = trim(basename($tokens[1], ' ').PHP_EOL); 
        }
      } else {
        echo "Tokens empty, somethings wrong";
      }
    }
    fclose($file);
    $x++;
  }
  
  // Adds URL to sites
  $y = 0;
  foreach ($sites as $site){                          
    foreach ($info as $i){
      if($site['name'] == $i['name']){
        $sites[$y]['url']=$i['url'];
      }
    }
    $y++;
  }
  return $sites;
}


//Parses trough _config.php for each directory in var/www to get
// SS_ENVIRONMENT_TYPE
function getSsStatus($sites){
  $z = 0;
  $path = '/var/www/';
  foreach($sites as $site){
    $newpath = $path.$site['name'].'/mysite/_config.php';
    unset($output);
    exec("cat $newpath | egrep 'SS\_ENVIRONMENT\_TYPE'", $output);
    if($output){                        // If the SS type was found, either test or dev      
	$status = $output[0];
      if(strpos($status, 'test')){
        $sites[$z]['status'] = "test";
      }
      if (strpos($status, 'dev')){
        $sites[$z]['status'] = "dev";
      }
    } else {                            // If not found status is live
      $sites[$z]['status'] = "live";
    }
    $z++;
  }
  return $sites;
}

// Goes trough directories in /var/ww
// Gets short version of git status
// If the result is empty, the status is up to date
function getGitStatus($sites) {
  $i = 0;
  $path = '/var/www/';
  foreach($sites as $site){
    $newpath = $path.$site['name'].'/';	
    unset($output);
    exec("cd $newpath; git remote update; git status -uno", $output);
	foreach($output as $gitStatus){	
		if($sites[$i]['gitstatus'] === null){
			if((strpos($gitStatus, "tial commit") !== false)
			|| (strpos($gitStatus, "not staged for commit") !== false)
			|| (strpos($gitStatus, "to be commited") !== false)){
				$sites[$i]['gitstatus'] = "needs commit";
			}
    		 	if((strpos($gitStatus, "nothing to commit") !== false)
    			|| (strpos($gitStatus, "up-to-date") !== false)){
      				$sites[$i]['gitstatus'] = "up to date";	
    			}
    			if(strpos($gitStatus, "branch is behind") !== false) {
      				$sites[$i]['gitstatus'] = "needs pull";
    			}
    			if(strpos($gitStatus, "branch is ahead") !== false){
				$sites[$i]['gitstatus'] = "needs push";
    			}
		}
	}
	if($sites[$i]['gitstatus'] === null){
		$sites[$i]['gitstatus'] = "unknown";
	}
    $i++;
  }
  return $sites;
}
?>
