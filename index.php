<?php

require "vendor/autoload.php";
include "newsite.php";
include "list_functions.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>SSadmin test</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="viewport" content="width=device-width, initial-scale-1">
	<link rel="stylesheet" href="components/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/app.css">
</head>
<body>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="components/jquery/jquery.min.js"></script>
<script src="components/bootstrap/js/bootstrap.min.js"></script>


<script type="text/javascript">

$( document ).ready(function() {
    console.log( "ready!" );
    preload("images/loadingsmall.gif");
});

function showLoader () {
	$('#loadingContainer').show();
	$('#submissionForm').hide();
	
}
function hideLoader () {
	$('#loadingContainer').hide();
	$('#submissionForm').show();
}

</script>

<header>
	<div class="container"> 

		<div class="panel panel-default">
		  <!-- Default panel contents -->
		  <div class="panel-heading">SS-admin</div>
		  <div class="panel-body">
		    
			<div class="controls">
				<button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal" id="createButton" onclick="hideLoader();">Create new Website</button>
			</div>
		  </div>

		<!-- Table -->
		<table class="table table-striped">
			<thead>
				<tr>
					<div class="btn-group btn-group-justified">
						<th>
							<div class="col-xs">
								<button type="button" class="btn btn-large btn-block btn-default full-width">
								<span class="glyphicon glyphicon-folder-open"></span>  Name </button>
							</div>
						</th>
						<th>
							<div class="col-xs">
								<button type="button" class="btn btn-large btn-block btn-default full-width">
								<span class="glyphicon glyphicon-globe"></span> Address </button>
							</div>
						</th>
						<th>
							<div class="col-xs">
								<button type="button" class="btn btn-large btn-block btn-default full-width">
								<span class="glyphicon glyphicon-save"></span>  GiT </button>
							</div>
						</th>
						<th>
							<div class="col-xs">
								<button type="button" class="btn btn-large btn-block btn-default full-width">
								<span class="glyphicon glyphicon-ok"></span> Status </button>
							</div>
						</th>
					</div>
				</tr>
			</thead>
			<tbody>

				<?php
				foreach($sites as $num => $site) {
				echo "<tr>";
				echo "<td>".$site['name']. "</td>";
				echo "<td><a target='_blank' href='//".$site['url']."/'>".$site['url']."</a></td>";
				echo '<td class="git">'.$site['gitstatus'].'</td>';
				echo '<td class="status">'.$site['status'].'</td>';
				echo "</tr>";
				}
				?>
			</tbody>
		</table>

		</div>
	</div>
</header>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Create Website</h4>
      </div>

	<div class="modal-body">

	<form class="form-horizontal" action="index.php" method="post" id="submissionForm">
		<fieldset>

			<!-- Text input-->
			<div class="control-group">
			<label class="control-label" for="textinput">Website/folder Name</label>
				<div class="controls">
					<input id="siteName" name="siteName" type="text" placeholder="Innit" class="input-xlarge modulInput" >
					<p class="help-block">Same as Git repository name</p>
				</div>
			</div>

			<!-- Text input-->
			<div class="control-group">
			<label class="control-label" for="textinput">URL Address</label>
				<div class="controls">
					<input id="urlInput" name="urlInput" type="text" placeholder="www.innit.no" class="input-xlarge modulInput" >
					<p class="help-block"> </p>
				</div>
			</div>

			<!-- Text input-->
			<div class="control-group">
			<label class="control-label" for="textinput">Git repository URL</label>
				<div class="controls">
					<input id="gitInput" name="gitInput" type="text" placeholder="www.git.no/1234" class="input-xlarge modulInput"  >
					<p class="help-block"></p>
				</div>
			</div>

			<!-- Text input-->
			<div class="control-group">
			<label class="control-label" for="textinput">Database name</label>
				<div class="controls">
					<input id="dbInput" name="dbInput" type="text" placeholder="innit.db" class="input-xlarge modulInput" >
					<p class="help-block"></p>
				</div>
			</div>
		</fieldset>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="cancelButton" data-dismiss="modal">Cancel</button>

        <!-- Calls the createProject function in functions.php -->
        <input type="submit" class="btn btn-default " id="submitButton" name="submitButton" value="Create" onclick="showLoader();" />
        
        
        
    
    
        <p> 
</div>
      
    
	</form>
      
    <div class="container" id="loadingContainer" style="width:200px">
     
	<img src="images/loadingsmall.gif" alt="Loading" />

</div>
 

</body>
</html>
