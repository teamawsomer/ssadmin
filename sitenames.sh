		#!/bin/bash
# Script for getting websitenames from var/www
cd /var/www
i=0
while read line
do
	names[$i]="${line%?}"	#Removes the /
	((i++))
done < <(ls -d */)
printf -- '%s\n' "${names[@]}"
