#!/bin/bash

WEBROOT="/var/www/"
VHOSTDIR="/etc/apache2/sites-available/"
SYMLINKDIR="/etc/apache2/sites-enabled/"
EXTENSION=".dev"
RELOADAPACHE="/etc/init.d/apache2 reload"
GETCOMPOSER="http://getcomposer.org/installer"

#Creates new vhost file by copying skeleton file and replace with new data
cp "$VHOSTDIR/skeleton" "$VHOSTDIR$1$EXTENSION"
echo "created $VHOSTDIR$1$EXTENSION"
find "$VHOSTDIR$1$EXTENSION" -type f -exec sed -i "s/SKELETONNAME/$1/" {} \;
find "$VHOSTDIR$1$EXTENSION" -type f -exec sed -i "s/SKELETONURL/$2/" {} \;

# Makes folder in var/www
mkdir "$WEBROOT$1/"
chown -R apache:apache "$WEBROOT$1/"

# Symlinks the new site with the new url
cd "$SYMLINKDIR"
ln -s ../sites-available/"$1$EXTENSION"

# Reloads apache
sudo a2ensite $1
sudo /etc/init.d/apache2 reload

# Gets composer and git repo
cd "$WEBROOT$1"
curl -sS "$GETCOMPOSER" | php
git init
git remote add origin $3
git pull origin master
php composer.phar install

#Creates database
SQL="CREATE DATABASE IF NOT EXISTS $4; GRANT ALL PRIVILEGES ON $4.* TO '$4'@'localhost'; FLUSH PRIVILEGES;"
mysql -uroot -pwebprosjekter -e "$SQL"
